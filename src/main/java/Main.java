import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import org.json.XML;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Main {
    private static String XML_FILE= "src/main/resources/sample.xml";
    private static String YAML_FILE = "src/main/resources/sample.yaml";

    public static void main (String[] args)  {

        try {
            String json = XML.toJSONObject(Files.readString(Path.of(XML_FILE))).toString();
            String yaml = jsonToYaml(json);
            Files.writeString(Path.of(yaml), XML_FILE);
        } catch (IOException o) {
            o.getMessage();
        }
    }

    //methods for convertation and cleaning Json String
    public static String jsonToYaml(String yamlFile) throws JsonProcessingException {
        JsonNode nodeTree = new ObjectMapper().readTree(yamlFile);
        deleteTechRegs(nodeTree);
        return new YAMLMapper().writeValueAsString(nodeTree);
    }

    private static void deleteTechRegs(JsonNode jsonNodeTree) {
        JsonNode nodeParent = jsonNodeTree.get("Product");
        if (nodeParent == null || nodeParent.get("techRegs") == null) {
            return;
        }
        ((ObjectNode) nodeParent).remove("techRegs");
    }
}
